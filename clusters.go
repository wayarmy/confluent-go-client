package confluent

import (
	"encoding/json"
)

const (
	clusterUri = "/kafka/v3/clusters"
)

type Related struct {
	Related string `json:"related"`
}

type GetKafkaClusterListData struct {
	Kind                   string  `json:"kind"`
	ClusterID              string  `json:"cluster_id"`
	Controller             Related `json:"controller"`
	Acls                   Related `json:"acls"`
	Brokers                Related `json:"brokers"`
	BrokerConfigs          Related `json:"broker_configs"`
	ConsumerGroups         Related `json:"consumer_groups"`
	Topics                 Related `json:"topics"`
	PartitionReassignments Related `json:"partition_reassignments"`
}

type GetKafkaClusterList struct {
	Kind     string                    `json:"kind"`
	Metadata Metadata                  `json:"metadata"`
	Data     []GetKafkaClusterListData `json:"data"`
}

type GetKafkaCluster struct {
	Kind     string `json:"kind"`
	Metadata struct {
		Self string `json:"self"`
		Next string `json:"next"`
	} `json:"metadata"`
	ClusterID              string  `json:"cluster_id"`
	Controller             Related `json:"controller"`
	Acls                   Related `json:"acls"`
	Brokers                Related `json:"brokers"`
	BrokerConfigs          Related `json:"broker_configs"`
	ConsumerGroups         Related `json:"consumer_groups"`
	Topics                 Related `json:"topics"`
	PartitionReassignments Related `json:"partition_reassignments"`
}

func (c *Client) ListCluster() ([]GetKafkaClusterListData, error) {
	resp, err := c.DoRequest("GET", clusterUri, nil)
	if err != nil {
		return nil, err
	}

	var clustersResp GetKafkaClusterList

	err = json.Unmarshal(resp, &clustersResp)
	if err != nil {
		return nil, err
	}

	return clustersResp.Data, nil
}

func (c *Client) GetCluster(clusterId string) (*GetKafkaCluster, error) {
	pathUri := clusterUri + "/" + clusterId
	resp, err := c.DoRequest("GET", pathUri, nil)
	if err != nil {
		return nil, err
	}

	var clustersResp *GetKafkaCluster

	err = json.Unmarshal(resp, &clustersResp)
	if err != nil {
		return nil, err
	}

	return clustersResp, nil
}
