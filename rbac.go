package confluent

import (
	"bytes"
	"encoding/json"
)

const (
	principalPath = "/security/1.0/principals/"
)

// ListClustersActive active in Confluent system
// Support:
// - kafka cluster
// - Kafka connect cluster
// - KSql cluster
// - Schema Registry cluster
type ListClustersActive struct {
	// Kafka cluster ID
	KafkaCluster string `json:"kafka-cluster,omitempty"`

	// Kafka Connect Cluster ID
	ConnectCluster string `json:"connect-cluster,omitempty"`

	// kSQL cluster ID
	KSqlCluster string `json:"ksql-cluster,omitempty"`

	// Schema Registry Cluster ID
	SchemaRegistryCluster string `json:"schema-registry-cluster,omitempty"`
}

type ClusterDetails struct {
	ClusterName string             `json:"clusterName,omitempty"`
	Clusters    ListClustersActive `json:"clusters"`
}

type RoleBindings struct {
	ResourceType string `json:"resourceType"`
	Name         string `json:"name"`
	PatternType  string `json:"patternType"`
}

type UpdateRoleBinding struct {
	Scope            ClusterDetails `json:"scope"`
	ResourcePatterns []RoleBindings `json:"resourcePatterns"`
}

// BindPrincipalToRole will bind the principal to a cluster-scoped role for a specific cluster or in a given scope
func (c *Client) BindPrincipalToRole(principal, roleName string, cDetails ClusterDetails) error {
	u := principalPath + principal + "/roles/" + roleName

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(cDetails)

	_, err := c.DoRequest("POST", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}

// DeleteRoleBinding remove the role (cluster or resource scoped) from the principal at the give scope/cluster
func (c *Client) DeleteRoleBinding(principal, roleName string, cDetails ClusterDetails) error {
	u := principalPath + principal + "/roles/" + roleName

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(cDetails)

	_, err := c.DoRequest("DELETE", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}

// LookupRoleBinding will lookup the role-bindings for the principal at the given scope/cluster using the given role
func (c *Client) LookupRoleBinding(principal, roleName string, cDetails ClusterDetails) ([]RoleBindings, error) {
	u := principalPath + principal + "/roles/" + roleName + "/resources"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(cDetails)

	r, err := c.DoRequest("POST", u, payloadBuf)
	if err != nil {
		return nil, err
	}

	var roleBindings []RoleBindings

	err = json.Unmarshal(r, &roleBindings)
	if err != nil {
		return nil, err
	}
	return roleBindings, nil
}

// IncreaseRoleBinding : incrementally grant the resources to the principal at the given scope/cluster using the given role
func (c *Client) IncreaseRoleBinding(principal, roleName string, uRoleBinding UpdateRoleBinding) error {
	u := principalPath + principal + "/roles/" + roleName + "/bindings"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(uRoleBinding)

	_, err := c.DoRequest("POST", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}

// DecreaseRoleBinding : Incrementally remove the resources from the principal at the given scope/cluster using the given role
func (c *Client) DecreaseRoleBinding(principal, roleName string, uRoleBinding UpdateRoleBinding) error {
	u := principalPath + principal + "/roles/" + roleName + "/bindings"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(uRoleBinding)

	_, err := c.DoRequest("DELETE", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}

// OverwriteRoleBinding will overwrite existing resource grants
func (c *Client) OverwriteRoleBinding(principal, roleName string, uRoleBinding UpdateRoleBinding) error {
	u := principalPath + principal + "/roles/" + roleName + "/bindings"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(uRoleBinding)

	_, err := c.DoRequest("PUT", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}
