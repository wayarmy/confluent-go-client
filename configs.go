package confluent

import (
	"bytes"
	"encoding/json"
)

type TopicConfigsResponse struct {
	Kind     string               `json:"kind"`
	Metadata Metadata             `json:"metadata"`
	Data     []TopicConfigElement `json:"data"`
}

type TopicConfigElement struct {
	Kind        string     `json:"kind"`
	Metadata    Metadata   `json:"metadata"`
	ClusterId   string     `json:"cluster_id"`
	TopicName   string     `json:"topic_name"`
	Name        string     `json:"name"`
	Value       string     `json:"value"`
	IsDefault   bool       `json:"is_default"`
	IsReadOnly  bool       `json:"is_read_only"`
	IsSensitive bool       `json:"is_sensitive"`
	Source      string     `json:"source"`
	Synonyms    []Synonyms `json:"synonyms"`
}

type Synonyms struct {
	Name      string `json:"name"`
	Value     string `json:"value,omitempty"`
	Source    string `json:"source,omitempty"`
	Operation string `json:"operation,omitempty"`
}

func (c *Client) GetTopicConfigs(clusterId string, topicName string) ([]TopicConfigElement, error) {
	u := "/clusters/" + clusterId + "/topics/" + topicName + "/configs"

	r, err := c.DoRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}

	var topicConfResp *TopicConfigsResponse
	err = json.Unmarshal(r, &topicConfResp)
	if err != nil {
		return nil, err
	}

	return topicConfResp.Data, nil
}

func (c *Client) UpdateTopicConfigs(clusterId string, topicName string, data []Synonyms) error {
	u := "/clusters/" + clusterId + "/topics/" + topicName + "/configs:alter"

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(data)

	_, err := c.DoRequest("POST", u, payloadBuf)
	if err != nil {
		return err
	}
	return nil
}
