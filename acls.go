package confluent

import (
	"bytes"
	"encoding/json"
)

const (
	aclsPath = "acls"
)

type AclInformation struct {
	Kind     string `json:"kind"`
	Metadata struct {
		Self string `json:"self"`
	} `json:"metadata"`
	ClusterId    string `json:"cluster_id"`
	ResouceType  string `json:"resouce_type"`
	ResourceName string `json:"resource_name"`
	PatternType  string `json:"pattern_type"`
	Principal    string `json:"principal"`
	Host         string `json:"host"`
	Operation    string `json:"operation"`
	Permission   string `json:"permission"`
}

type KafkaAclList struct {
	Kind     string `json:"kind"`
	Metadata struct {
		Self string `json:"self"`
	} `json:"metadata"`
	Data []AclInformation `json:"data"`
}

type CreateAclConfigs struct {
	ResourceType string `json:"resource_type,omitempty"`
	ResourceName string `json:"resource_name,omitempty"`
	PatternType  string `json:"pattern_type,omitempty"`
	Principal    string `json:"principal,omitempty"`
	Host         string `json:"host,omitempty"`
	Operation    string `json:"operation,omitempty"`
	Permission   string `json:"permission,omitempty"`
}

func (c *Client) ListAcls(clusterId string) ([]AclInformation, error) {
	u := "/clusters/" + clusterId + "/" + aclsPath
	r, err := c.DoRequest("GET", u, nil)
	if err != nil {
		return nil, err
	}

	acls := KafkaAclList{}

	err = json.Unmarshal(r, &acls)
	if err != nil {
		return nil, err
	}

	return acls.Data, nil
}

func (c *Client) CreateAcl(clusterId string, aclConfig *CreateAclConfigs) error {
	u := "/clusters/" + clusterId + "/" + aclsPath

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(aclConfig)
	_, err := c.DoRequest("POST", u, payloadBuf)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) DeleteAcl(clusterId, resourceName string) error {
	u := "/clusters/" + clusterId + "/" + aclsPath

	aclDetele := CreateAclConfigs{
		ResourceName: resourceName,
	}

	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(aclDetele)
	_, err := c.DoRequest("DELETE", u, payloadBuf)
	if err != nil {
		return err
	}

	return nil
}
